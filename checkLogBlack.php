<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $customerDetails = getCustomerDetails($conn," WHERE no_of_call >= 3 AND status = 'BAD' ORDER BY last_updated DESC LIMIT 500");

$customerDetails = getCustomerDetails($conn," WHERE no_of_call >= 3 AND status = 'BAD' ORDER BY last_updated DESC LIMIT 4000");

$page = $_SERVER['PHP_SELF'];
$sec = "30";

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Customer Details (Black List) | adminTele" />
    <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
    <title>Customer Details (Black List) | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

    <h1 class="h1-title customer-h1">Customer</h1>

    <div id="divDataUpdated">

    </div>

    <!-- <div class="right-data-div">
        5 Data Updated<br>
        10 <b class="green-text">Good</b> updated<br>
        8 <b class="pink-text">Bad</b> updated
    </div> -->

    <div class="clear"></div>

    <!-- <h2 class="tab-h2">Customer Details (Black List) | <a href="checkLog.php" class="red-link">Customer Details</a> </h2> -->
    <!-- <h2 class="tab-h2"><a href="checkLog.php" class="red-link">Customer Details </a> | <a href="checkLogGood.php" class="red-link">Customer Details (Good)</a> | Customer Details (Black List) </a> </h2> -->
    <h4 class="tab-h2"><a href="checkLog.php" class="red-link">Customer Details </a> | <a href="checkLogUpdated.php" class="red-link">Updated</a> | <a href="checkLogGood.php" class="red-link">Good</a> | Black List</h4>
  <div class="big-four-input-container">
      <div class="four-input-div first-four-div">
        <p class="input-top-p">Phone Number</p>
        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Phone Number" class="tele-four-input tele-input clean">
      </div>
      <!-- <input type="text" id="myInputA" onkeyup="myFunctionA()" placeholder="Last Updated Date" class="search-input second-search-input"> -->
    
      <form method='post' action=''> 
        <?php
        if(isset($_POST['reset']))
        {
        ?>
        <div class="four-input-div left-four-input">
            <p class="input-top-p">From Date</p>
            <input type='text' class='dateFilter tele-four-input tele-input clean' name='fromDate'>
        </div>
        <div class="four-input-div right-four-input">
            <p class="input-top-p">To Date</p>
            <input type='text' class='dateFilter tele-four-input tele-input clean' name='endDate' >
        </div>    
        <?php
        }
        else
        {
        ?>
        <div class="four-input-div left-four-input">
            <p class="input-top-p">From Date</p>
            <input type='text' class='dateFilter tele-four-input tele-input clean' name='fromDate' value='<?php if(isset($_POST['fromDate'])) echo $_POST['fromDate']; ?>'>
        </div>
        <div class="four-input-div right-four-input">
            <p class="input-top-p">To Date</p>
            <input type='text' class='dateFilter tele-four-input tele-input clean' name='endDate' value='<?php if(isset($_POST['endDate'])) echo $_POST['endDate']; ?>'>
        </div>
        <?php
        }
        ?>
        <div class="four-input-div last-four-div">
            <input type='submit' name='but_search' value='Search' class="submit-btn clean">
            <input type='submit' name='reset' value='Reset' class="submit-btn clean right-submit-btn">
        </div>
      </form>
	</div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <!-- <table class="shipping-table"> -->
                <table class="shipping-table" id="myTable">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>NAME</th>
                            <th>PHONE</th>
                            <th>EMAIL</th>
                            <th>STATUS</th>
                            <th>REMARK</th>
                            <!-- <th>SPECIAL STATUS</th> -->
                            <th>LAST UPDATED</th>
                            <th>REVIEW</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $conn = connDB();
                        // Date filter
                        if(isset($_POST['but_search']))
                        {
                          $fromDate = $_POST['fromDate'];
                          $endDate = $_POST['endDate'];
                          if(!empty($fromDate) && !empty($endDate))
                          {
                            // $customerDetails = getCustomerDetails($conn," WHERE no_of_call >= 3 AND status = 'BAD' ORDER BY last_updated DESC LIMIT 4000");
                            $customerDetails = getCustomerDetails($conn," WHERE no_of_call >= 3 AND status = 'BAD' ORDER BY last_updated DESC LIMIT 4000 AND last_updated between '".$fromDate."' and '".$endDate."'");
                          }
                        }
                        elseif (isset($_POST['reset']))
                        {
                          $customerDetails = getCustomerDetails($conn," WHERE no_of_call >= 3 AND status = 'BAD' ORDER BY last_updated DESC LIMIT 4000");
                        }
                        else
                        {
                          $customerDetails = getCustomerDetails($conn," WHERE no_of_call >= 3 AND status = 'BAD' ORDER BY last_updated DESC LIMIT 4000");
                          // $customerDetails = getCustomerDetails($conn," WHERE no_of_call > 0 AND no_of_call <=2 AND status != 'Good' ORDER BY last_updated DESC LIMIT 4000");
                        }

                        if($customerDetails)
                        {   
                            for($cnt = 0;$cnt < count($customerDetails) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <!-- <td><?php //echo $customerDetails[$cnt]->getId();?></td> -->
                                <td><?php echo $customerDetails[$cnt]->getName();?></td>
                                <td><?php echo $customerDetails[$cnt]->getPhone();?></td>
                                <td><?php echo $customerDetails[$cnt]->getEmail();?></td>
                                <td><?php echo $customerDetails[$cnt]->getStatus();?></td>
                                <td><?php echo $customerDetails[$cnt]->getRemark();?></td>
                                <!-- <td><?php //echo $customerDetails[$cnt]->getUpdateStatus();?></td> -->

                                <!-- <td><?php //echo $customerDetails[$cnt]->getLastUpdated();?></td> -->
                                <td><?php echo date("d-m-Y",strtotime($customerDetails[$cnt]->getLastUpdated()));?></td>

                                <td>
                                    <form action="reviewCustomerDetailsBlack.php" method="POST">
                                        <button class="clean hover1 img-btn" type="submit" name="customer_name" value="<?php echo $customerDetails[$cnt]->getPhone();?>">
                                            <img src="img/edit2.png" class="width100 hover1a" alt="Review" title="Review">
                                            <img src="img/edit3.png" class="width100 hover1b" alt="Review" title="Review">
                                        </button>
                                    </form>
                                </td>

                            <?php
                            }?>
                            </tr>
                        <?php
                        }
                        $conn->close();
                        ?>
                    </tbody>

                </table>
            </div>
    </div>

        
</div>
<style>
.customer-li{
	color:#bf1b37;
	background-color:white;}
.customer-li .hover1a{
	display:none;}
.customer-li .hover1b{
	display:block;}
</style>
<?php include 'js.php'; ?>
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/themes/hot-sneaks/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script>
  $(function()
  {
    $("#fromDate").datepicker(
    {
    dateFormat:'yy-mm-dd',
    changeMonth: true,
    changeYear:true,
    }

    );
  });
</script>

<script>
  $(function()
  {
    $("#toDate").datepicker(
    {
      dateFormat:'yy-mm-dd',
      changeMonth: true,
      changeYear:true,
    }
    );
  });
</script>

<script type='text/javascript'>
  $(document).ready(function()
  {
    $('.dateFilter').datepicker(
    {
      dateFormat: "yy-mm-dd"
    });
  });
</script>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script type="text/javascript">
$(document).ready(function()
{
    $("#divDataUpdated").load("adminDataUpdated.php");
setInterval(function()
{
    $("#divDataUpdated").load("adminDataUpdated.php");
}, 5000);
});
</script>

<!-- <script>
function myFunctionA() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputA");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[6];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script> -->

</body>
</html>