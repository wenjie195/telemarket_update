-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2020 at 02:41 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_admintele`
--

-- --------------------------------------------------------

--
-- Table structure for table `companyselection`
--

CREATE TABLE `companyselection` (
  `id` bigint(20) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companyselection`
--

INSERT INTO `companyselection` (`id`, `company_name`, `date_created`, `date_updated`) VALUES
(1, 'Company A', '2020-02-23 19:01:06', '2020-02-23 19:01:06'),
(2, 'Company B', '2020-02-23 19:01:06', '2020-02-23 19:01:06'),
(3, 'Company C', '2020-02-23 19:01:28', '2020-02-23 19:01:28'),
(4, 'Company D', '2020-02-23 19:01:28', '2020-02-23 19:01:28');

-- --------------------------------------------------------

--
-- Table structure for table `customerdetails`
--

CREATE TABLE `customerdetails` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tele_name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `last_updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `no_of_call` varchar(255) DEFAULT '0',
  `update_status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customerdetails`
--

INSERT INTO `customerdetails` (`id`, `name`, `phone`, `email`, `tele_name`, `status`, `remark`, `company_name`, `type`, `reason`, `last_updated`, `no_of_call`, `update_status`, `date_created`, `date_updated`) VALUES
(1, 'GK', '1', 'terstegen@barca.cc', 'Tele1', 'Good', 'zzzz', 'Company B', 'Other', 'Other 1, Other 2', '2020-03-02 09:45:47', '1', NULL, '2020-03-02 09:07:00', '2020-03-02 09:45:47'),
(2, '', '2', 'semedo@barca.cc', 'Tele1', '', '', '', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(3, '', '3', 'pique@barca.cc', 'Tele1', '', '', '', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(4, '', '4', 'rakitic@barca.cc', 'Tele1', '', '', '', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(5, '', '5', 'sergio@barca.cc', 'Tele1', '', '', '', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(6, 'todibo', '6', 'todibo@barca.cc', 'Tele2', 'Good', 'well done', 'Company C', 'Good', 'Good 1, Good 2', '2020-03-02 09:31:50', '1', NULL, '2020-03-02 09:07:00', '2020-03-02 09:31:50'),
(7, 'arthur', '8', 'arthur@barca.cc', 'Tele2', 'Bad', 'damn bad', 'Company C', 'Bad', 'Bad 1, Bad 2, Bad 3', '2020-03-02 09:32:10', '1', NULL, '2020-03-02 09:07:00', '2020-03-02 09:32:10'),
(8, 'suarez diu', '9', 'suqrez@baarca.cc', 'Tele2', 'Good', 'finally ans', 'Company B', 'Other', 'Other 2, Other 3', '2020-03-02 09:57:08', '5', NULL, '2020-03-02 09:07:00', '2020-03-02 09:57:08'),
(9, '', '10', 'messi@barca.cc', 'Tele2', '', '', '', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(10, '', '11', 'dembele@barca.cc', 'Tele2', '', '', '', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `emgc_sos`
--

CREATE TABLE `emgc_sos` (
  `id` bigint(20) NOT NULL,
  `link` varchar(255) NOT NULL,
  `type` int(20) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emgc_sos`
--

INSERT INTO `emgc_sos` (`id`, `link`, `type`, `date_created`, `date_updated`) VALUES
(1, '<script>window.location=\'../teleDashboard.php\'</script>', 1, '2020-01-17 15:35:22', '2020-02-04 07:18:08'),
(2, 'header(\'Location: https://bigdomain.my\');', 2, '2020-01-17 15:44:58', '2020-01-17 15:44:58'),
(3, '  ?><?php echo $userDetails->getLink();?><?php', 3, '2020-01-17 15:51:10', '2020-01-17 15:51:26'),
(4, 'header(\'Location: https://www.facebook.com\');', 4, '2020-01-17 15:51:10', '2020-01-17 15:51:28'),
(5, 'echo \"<meta http-equiv=Refresh content=1;url=https://bigdomain.my/>\";', 5, '2020-01-17 15:53:20', '2020-01-17 15:53:20'),
(6, '<meta http-equiv=Refresh content=0;url=https://amway.my/>;', 6, '2020-01-17 15:56:40', '2020-01-17 16:13:23'),
(7, 'echo \"<script>window.location=\'../teleDashboard.php\'</script>\";', 7, '2020-01-17 16:11:04', '2020-01-17 16:11:04');

-- --------------------------------------------------------

--
-- Table structure for table `excel`
--

CREATE TABLE `excel` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tele_name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `last_updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `no_of_call` varchar(255) DEFAULT '0',
  `update_status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `excel`
--

INSERT INTO `excel` (`id`, `name`, `phone`, `email`, `tele_name`, `status`, `remark`, `last_updated`, `no_of_call`, `update_status`, `date_created`, `date_updated`) VALUES
(1, '', '1', 'terstegen@barca.cc', 'Tele1', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(2, '', '2', 'semedo@barca.cc', 'Tele1', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(3, '', '3', 'pique@barca.cc', 'Tele1', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(4, '', '4', 'rakitic@barca.cc', 'Tele1', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(5, '', '5', 'sergio@barca.cc', 'Tele1', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(6, '', '6', 'todibo@barca.cc', 'Tele2', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(7, '', '8', 'arthur@barca.cc', 'Tele2', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(8, '', '9', 'suqrez@baarca.cc', 'Tele2', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(9, '', '10', 'messi@barca.cc', 'Tele2', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00'),
(10, '', '11', 'dembele@barca.cc', 'Tele2', '', '', '2020-03-02 09:07:00', '0', NULL, '2020-03-02 09:07:00', '2020-03-02 09:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `reason`
--

CREATE TABLE `reason` (
  `id` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `type` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reason`
--

INSERT INTO `reason` (`id`, `status`, `reason`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Good', 'Good 1', 1, '2020-02-19 20:00:28', '2020-02-19 20:00:28'),
(2, 'Good', 'Good 2', 1, '2020-02-19 20:00:28', '2020-02-19 20:00:28'),
(3, 'Good', 'Good 3', 1, '2020-02-19 20:01:10', '2020-02-19 20:01:10'),
(4, 'Bad', 'Bad 1', 1, '2020-02-20 17:26:18', '2020-02-20 17:26:32'),
(5, 'Bad', 'Bad 2', 1, '2020-02-19 20:01:39', '2020-02-20 17:17:33'),
(6, 'Bad', 'Bad 3', 1, '2020-02-19 20:01:39', '2020-02-20 17:17:41'),
(7, 'Other', 'Other 1', 1, '2020-02-19 20:02:16', '2020-02-19 20:02:16'),
(8, 'Other', 'Other 2', 1, '2020-02-19 20:02:16', '2020-02-19 20:02:16'),
(9, 'Other', 'Other 3', 1, '2020-02-19 20:03:00', '2020-02-19 20:03:00'),
(10, 'No Take Call', 'No Take Call 1', 1, '2020-02-19 20:03:00', '2020-02-19 20:03:00'),
(11, 'No Take Call', 'No Take Call 2', 1, '2020-02-19 20:03:20', '2020-02-19 20:03:20'),
(12, 'No Take Call', 'No Take Call 3', 1, '2020-02-19 20:03:20', '2020-02-19 20:03:20');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `id` bigint(20) NOT NULL,
  `link` varchar(255) NOT NULL,
  `type` int(20) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`id`, `link`, `type`, `date_created`, `date_updated`) VALUES
(1, '<meta http-equiv=Refresh content=0;url=https://amway.my/>;', 0, '2020-01-17 07:35:22', '2020-02-04 08:24:55'),
(2, '<url=https://bigdomain.my/>', 1, '2020-01-17 07:44:58', '2020-02-04 08:42:49'),
(8, '\"$_SERVER[\'PHP_SELF\']\"', 3, '2020-02-04 08:30:29', '2020-02-04 08:39:24');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Good', 1, '2020-02-19 20:00:28', '2020-02-19 20:00:28'),
(2, 'Bad', 1, '2020-02-19 20:01:10', '2020-02-19 20:36:28'),
(3, 'Other', 1, '2020-02-19 20:02:16', '2020-02-19 20:36:47'),
(4, 'No Take Call', 1, '2020-02-19 20:03:00', '2020-02-19 20:36:58');

-- --------------------------------------------------------

--
-- Table structure for table `time_teleupdate`
--

CREATE TABLE `time_teleupdate` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `tele_name` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `update_status` varchar(255) DEFAULT NULL,
  `update_remark` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `reason` varchar(25) DEFAULT NULL,
  `no_of_update` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_teleupdate`
--

INSERT INTO `time_teleupdate` (`id`, `uid`, `tele_name`, `customer_name`, `update_status`, `update_remark`, `company_name`, `type`, `reason`, `no_of_update`, `date_created`, `date_updated`) VALUES
(1, '6fd68af94270ba1521e05e94c28c7914', 'Tele2', 'todibo', 'Good', 'well done', 'Company C', 'Good', 'Good 1, Good 2', '1', '2020-03-02 09:31:50', '2020-03-02 09:31:50'),
(2, '6fd68af94270ba1521e05e94c28c7914', 'Tele2', 'arthur', 'Bad', 'damn bad', 'Company C', 'Bad', 'Bad 1, Bad 2, Bad 3', '1', '2020-03-02 09:32:10', '2020-03-02 09:32:10'),
(3, '6fd68af94270ba1521e05e94c28c7914', 'Tele2', 'suarez', 'Bad', 'asd', 'Company B', 'Other', 'Other 1, Other 2', '1', '2020-03-02 09:40:30', '2020-03-02 09:53:06'),
(4, '6fd68af94270ba1521e05e94c28c7914', 'Tele2', 'suarez', 'Bad', 'asdasd', 'Company B', 'No Take Call', 'No Take Call 1, No Take C', '2', '2020-03-02 09:40:42', '2020-03-02 09:53:08'),
(5, '6fd68af94270ba1521e05e94c28c7914', 'Tele2', 'suarez', 'Bad', 'asdasdasd', 'Company B', 'Other', 'Other 2', '3', '2020-03-02 09:40:52', '2020-03-02 09:53:10'),
(6, '921a1fea68bdb61c5b21b99329a59432', 'Tele1', 'GK', 'Good', 'zzzz', 'Company B', 'Other', 'Other 1, Other 2', '1', '2020-03-02 09:45:47', '2020-03-02 09:45:47'),
(7, '6fd68af94270ba1521e05e94c28c7914', 'Tele2', 'suarez', 'Good', 'finally', NULL, NULL, NULL, '4', '2020-03-02 09:49:00', '2020-03-02 09:52:55'),
(8, '6fd68af94270ba1521e05e94c28c7914', 'Tele2', 'suarez', 'Good', 'finally ans', NULL, 'Other', 'Other 2, Other 3', '5', '2020-03-02 09:57:08', '2020-03-02 09:57:08');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `address`, `nationality`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9ca817799c575c1afd37cccf6a22aa7f', 'admin', 'admin@gmail.com', 'fe8192029d7b39d66fe035ea96623f6ab6f06c961985d12f210b353764b1e90a', 'e74df95cbd449eb12c45c8685694897a6869037f', '12351236', 'admin', NULL, NULL, 1, 0, '2020-01-14 06:36:33', '2020-01-14 08:28:41'),
(3, '921a1fea68bdb61c5b21b99329a59432', 'Tele1', 'tele1@gg.cc', '07caa962fee48e8e235bb57dc8de83651e89e0cd8f8edddf407124749f776ae9', 'e80aeb236a5528ad29d82be3ac60b78490d0a2ea', '123456', 'Tele1', 'qwert', NULL, 1, 1, '2020-02-04 04:23:57', '2020-02-05 04:02:11'),
(4, '6fd68af94270ba1521e05e94c28c7914', 'Tele2', 'tele2@gg.cc', 'd1794d9b452ae10d1b135c1c05da98b0b2170ef2c1280b8d254239b8a420435a', 'fe04ef8fc9368dcbe8be35855763d4c24641f5d5', '456789', 'Tele2', 'asdasd', NULL, 1, 1, '2020-02-05 04:01:57', '2020-02-05 04:01:57'),
(5, '5db53888e74f79ed48f60d4c66b97ceb', 'asd', 'asd@gg.cc', '7c151da70fe99916ff84b29935d783f8f8e6472e5de4c0c9554603fe5fb05710', 'f3642f33f8bdab50e65ba508b611e502a8f2d264', '+0123', 'asd', 'asdasd', NULL, 1, 0, '2020-02-06 08:21:43', '2020-02-06 08:21:43'),
(6, '5bcc6a60390424130589c17b6a0edee6', 'Tele3', 'tele3@gg.cc', '763234b60620ef99acff04c7139c65124118fc774554cb29d166e34f417e7e1f', '1ae7f87c13de54cc352e371195d998bef4607ec3', '+123-123', 'Tele3', 'tele3 address', NULL, 1, 1, '2020-02-12 02:35:24', '2020-02-12 02:35:24'),
(7, 'baae03b8098d3ddbd488019a681eb85b', 'Tele4', 'tele4@gg.cc', '43aa6e6428ece9a3ae8247d777c0669fa2387753292167a6f6ad90088f777c03', '0dfc4a57466648ec9da8f4fa016d237fc9af48e7', '+456-456', 'Tele4', 'tele4 address', NULL, 1, 1, '2020-02-12 02:35:45', '2020-02-12 02:35:45'),
(8, 'e70733c30e69843b511c90efd5c76257', 'Tele5', 'tele5@gg.cc', '603269afd3544c2856aa6646e56c4001b04405058f8632c5ec1175cf213e787a', '4aaf526a7f7fcbef595de016df46f951428b9efa', '+789-789', 'Tele5', 'tele5 address', NULL, 1, 1, '2020-02-12 02:36:09', '2020-02-12 02:36:09'),
(9, '2fd640656641623527f1165315459bca', 'Tele6', 'tele6@gg.cc', 'f046998ba364505bf14d86a684f31aa4e2b7f1708bd2d10216be431121116860', '518a8251d9c16482fa5602e30c33392b493ec9cf', '+741-741', 'Tele6', 'tele6 address', NULL, 1, 1, '2020-02-12 02:36:35', '2020-02-12 02:36:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companyselection`
--
ALTER TABLE `companyselection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customerdetails`
--
ALTER TABLE `customerdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emgc_sos`
--
ALTER TABLE `emgc_sos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excel`
--
ALTER TABLE `excel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reason`
--
ALTER TABLE `reason`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_teleupdate`
--
ALTER TABLE `time_teleupdate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companyselection`
--
ALTER TABLE `companyselection`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customerdetails`
--
ALTER TABLE `customerdetails`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `emgc_sos`
--
ALTER TABLE `emgc_sos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `excel`
--
ALTER TABLE `excel`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `reason`
--
ALTER TABLE `reason`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `time_teleupdate`
--
ALTER TABLE `time_teleupdate`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
